import { Service } from "@enitoni/gears-discordjs"
import { TextChannel, MessageEmbed } from "discord.js"
import { bind } from "decko"

import { ChanceService } from "./ChanceService"

const LOG_CHANNEL_ID = "486622026493067297"

export class ChannelLoggingService extends Service {
	logsChannel!: TextChannel

	async serviceDidStart() {
		const channel = this.bot.client.channels.cache.get(LOG_CHANNEL_ID)
		if (!(channel instanceof TextChannel))
			throw new Error("ID isn't from a text channel.")

		this.logsChannel = channel
		this.subscribeEvents()
		this.logReady()
	}

	@bind
	subscribeEvents() {
		const { bot } = this
		bot.client.on("ready", this.logReady)
	}

	@bind
	async logReady() {
		const { bot, manager, logsChannel } = this

		const botUser = bot.client.user
		const { defaultConfig } = manager.getService(ChanceService)

		const embed = new MessageEmbed({
			color: Color.Blue,
			timestamp: new Date(),
		})

		embed.setAuthor(botUser!.tag, botUser!.avatarURL()!)
		embed.setTitle("Ready")
		embed.addField("Default Chance", defaultConfig.chance, true)
		embed.addField("Guild Count", bot.client.guilds.cache.size, true)

		logsChannel.send(embed)
	}
}

enum Color {
	Green = 0x1db954,
	Blue = 0x4687d6,
	Yellow = 0xfed859,
	Red = 0xff3939,
}
