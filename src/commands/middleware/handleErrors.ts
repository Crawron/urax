import { Permissions, MessageEmbed } from "discord.js"
import { Middleware } from "@enitoni/gears-discordjs"
import { PermissionError } from "../../types"
import { humanizePermissions } from "../../helpers/humanizePermissions"

export const handleErrors: () => Middleware = () => async (context, next) => {
	try {
		await next()
	} catch (err) {
		const { message } = context

		if (err instanceof PermissionError) {
			const { permissions } = err

			const missingPerms = message.member!.permissions.missing(permissions)
			const humanizedPerms = humanizePermissions(new Permissions(missingPerms))

			const embed = new MessageEmbed({
				description: "u r a denied. no permission, shoo",
				footer: { text: `${humanizedPerms} permission required` },
				color: 0xde3838,
			})

			message.channel.send(embed)
		}
	}
}
