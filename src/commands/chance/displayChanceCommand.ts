import { matchAlways } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"

import { ChanceService } from "../../services/ChanceService"
import { toPercentage } from "../../helpers/toPercentage"
import { getBotColor } from "../../helpers/getBotColor"
import { getResponseEmbed } from "../../helpers/getResponseEmbed"

export const displayChanceCommand = new Command({
	matcher: matchAlways(),
	middleware: [
		async context => {
			const { manager, message } = context

			const chanceServ = manager.getService(ChanceService)
			const { chance } = chanceServ.getGuildConfig(message.guild!.id)

			const botColor = getBotColor(context)
			const embed = getResponseEmbed(
				`u r a \`${toPercentage(chance)}\` chance`,
				botColor
			)

			message.channel.send(embed)
		},
	],
})
