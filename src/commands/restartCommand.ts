import { MessageEmbed } from "discord.js"
import { Command, matchPrefixes, matchAll } from "@enitoni/gears"

import { matchUser } from "./matchers/matchUser"

export const restartCommand = new Command({
	matcher: matchAll(matchUser("109677308410875904"), matchPrefixes("restart")),
	middleware: [
		async context => {
			const { bot, message } = context
			await message.channel.send(
				new MessageEmbed({ description: "restartin..." })
			)
			await bot.client.user.setStatus("invisible")
			process.exit(0)
		},
	],
})
