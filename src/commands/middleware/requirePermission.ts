import { PermissionResolvable, UserResolvable } from "discord.js"
import { Middleware } from "@enitoni/gears-discordjs"
import { PermissionError } from "../../types"

export const requirePermissions: (
	perms: PermissionResolvable[],
	superUsers?: string[]
) => Middleware = (perms, superUsers) => async (context, next) => {
	const { member, author } = context.message

	const hasPerms = member && member.permissions.has(perms)
	const isSudo = superUsers && superUsers.includes(author.id)

	if (!(hasPerms || isSudo)) throw new PermissionError(context, perms)

	return next()
}
