import { parseChance } from "./parseChance"

test("parse chance", () => {
	expect(parseChance("12%")).toBe(0.12)
	expect(parseChance(".11")).toBe(0.11)
	expect(parseChance("32,5")).toBe(0.325)
	expect(parseChance("5,10")).toBe(0.051)
	expect(parseChance(",55")).toBe(0.55)
	expect(parseChance("2,")).toBe(0.02)
	expect(parseChance("25")).toBe(0.25)
	expect(parseChance("1%")).toBe(0.01)
	expect(parseChance("0.5%")).toBe(0.005)
})

test("weird cases", () => {
	expect(parseChance("DeffinitelyNaN")).toBe(NaN)
	expect(parseChance("some 50% weird way to give a 11 number")).toBe(0.5)
	expect(parseChance("0.1 ksjdkfjskl %")).toBe(0.1)
})
