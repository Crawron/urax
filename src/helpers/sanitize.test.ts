import { sanitize } from "./sanitize"

test("sanitize markdown", () => {
	const text =
		"_italics_ or *italics*. __underlined__, ~~striked~~ and ||spoiled?||? Maybe > quoted"

	expect(sanitize(text)).toBe(
		"italics or italics. underlined, striked and ? Maybe quoted"
	)
})

test("emojis intact", () => {
	const text = "this some :100: right there :point_right:"
	expect(sanitize(text)).toBe(text)
})

test("removed spoilers", () => {
	const text = "At the end ||the movie ends||"
	expect(sanitize(text)).toBe("At the end")
})

test("remove double and trailing spaces", () => {
	const text = "Something  with a few     spaces  "
	expect(sanitize(text)).toBe("Something with a few spaces")
})

test("removed multilines and tabs", () => {
	const text = `multiline
	text
	to single
	line`
	expect(sanitize(text)).toBe("multiline text to single line")
})
