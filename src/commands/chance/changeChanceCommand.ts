import { matchRegex, matchNone } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"

import { ChanceService } from "../../services/ChanceService"
import { parseChance } from "../../helpers/parseChance"
import { toPercentage } from "../../helpers/toPercentage"
import { getBotColor } from "../../helpers/getBotColor"
import { getResponseEmbed } from "../../helpers/getResponseEmbed"
import { requirePermissions } from "../middleware/requirePermission"

export const changeChanceCommand = new Command({
	matcher: matchNone(matchRegex(/^$/)),
	middleware: [
		requirePermissions(
			["MANAGE_MESSAGES"],
			["109677308410875904", "143419667677970434"]
		),
		async context => {
			const { content, manager, message } = context

			const chance = parseChance(content)
			const botColor = getBotColor(context)

			if (isNaN(chance)) {
				const embed = getResponseEmbed(
					`i see no number in whatever \`${content}\` is`,
					botColor
				)

				return message.channel.send(embed)
			}

			const chanceServ = manager.getService(ChanceService)
			await chanceServ.setGuildConfig(message.guild!.id, { chance })

			const newChance = chanceServ.getGuildConfig(message.guild!.id).chance

			const embed = getResponseEmbed(
				`u r a \`${toPercentage(newChance)}\` chance now`,
				botColor
			)

			return message.channel.send(embed)
		},
	],
})
