import { matchPrefixes } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"

import { getBotColor } from "../helpers/getBotColor"
import { getResponseEmbed } from "../helpers/getResponseEmbed"

export const helpCommand = new Command({
	matcher: matchPrefixes("help"),
	middleware: [
		async context => {
			const { message } = context

			const botColor = getBotColor(context)
			const embed = getResponseEmbed("", botColor, {
				".chance": `set or display the current server chance
\`.chance\`
\`.chance 1\` _= 100%_
\`.chance 0.5\` _= 50%_
\`.chance 5%\` _= 5%_
\`.chance 0.3%\` _= 0.3%_
`,
			})

			message.channel.send(embed)
		},
	],
})
