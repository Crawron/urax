import { Context } from "@enitoni/gears-discordjs"
import { PermissionResolvable } from "discord.js"

export class PermissionError extends Error {
	constructor(
		public context: Context,
		public permissions: PermissionResolvable,
		message?: string
	) {
		super(message)
	}
}
