import { Context } from "@enitoni/gears-discordjs"

export function getBotColor(context: Context): number {
	const { message, bot } = context

	const botMember = message.guild!.member(bot.client.user!)

	if (!botMember) return 0
	return botMember.displayColor
}
