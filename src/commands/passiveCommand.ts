import { matchAll, matchNone, matchRegex } from "@enitoni/gears"
import { Command } from "@enitoni/gears-discordjs"

import { matchChance } from "./matchers/matchChance"
import { matchUrl } from "./matchers/matchUrl"
import { sanitize } from "../helpers/sanitize"

export const passiveCommand = new Command({
	matcher: matchAll(matchChance(), matchNone(matchUrl(), matchRegex(/^$/))),

	action: async context => {
		const { channel, cleanContent } = context.message

		const word = sanitize(cleanContent).toLowerCase().split(" ").pop()

		if (word) channel.send(`u r a ${word}`)
	},
})
