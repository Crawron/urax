import { toPercentage } from "./toPercentage"

test("convert from normal", () => {
	expect(toPercentage(1)).toBe("100%")
	expect(toPercentage(0.11)).toBe("11%")
	expect(toPercentage(0.123)).toBe("12.3%")
	expect(toPercentage(0.0001)).toBe("0.01%")
	expect(toPercentage(0.0001224)).toBe("0.01224%")
	expect(toPercentage(0)).toBe("0%")
	expect(toPercentage(0.55)).toBe("55%")
})
