import { MessageEmbed } from "discord.js"

export function getResponseEmbed(
	description: string,
	color?: number,
	fields?: any
) {
	const embed = new MessageEmbed({ description })
	if (color) embed.setColor(color)

	for (let entry in fields) {
		embed.addField(entry, fields[entry])
	}

	return embed
}
