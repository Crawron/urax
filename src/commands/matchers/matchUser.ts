import { CommandMatcher } from "@enitoni/gears"

export const matchUser: (
	id: string
) => CommandMatcher = id => async context => {
	const { author } = context.message
	if (author.id === id) return context
}
