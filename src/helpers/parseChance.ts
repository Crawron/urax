export function parseChance(str: string): number {
	const valueRegex = /(?:\d*(\.|\,)?|(\.|\,))\d+%?/
	const valueStr = str.match(valueRegex)
	if (!valueStr) return NaN

	const parsed = parseFloat(valueStr[0].replace(",", "."))
	const isPercentage = valueStr[0].endsWith("%") || parsed > 1

	return Math.min(isPercentage ? parsed / 100 : parsed, 1)
}
