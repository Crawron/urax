import { matchPrefixes } from "@enitoni/gears"
import { CommandGroup } from "@enitoni/gears-discordjs"

import { changeChanceCommand } from "./changeChanceCommand"
import { displayChanceCommand } from "./displayChanceCommand"

export const chanceGroup = new CommandGroup({
	matcher: matchPrefixes("chance"),
	commands: [changeChanceCommand, displayChanceCommand],
})
