import { CommandMatcher } from "@enitoni/gears"

import { ChanceService } from "../../services/ChanceService"
import { roll } from "../../helpers/roll"

export const matchChance: () => CommandMatcher = () => async context => {
	const chanceService = context.manager.getService(ChanceService)

	const { guild } = context.message
	const guildConfig = chanceService.getGuildConfig(guild.id)

	if (roll(guildConfig.chance)) return context
}
