import { matchPrefixes } from "@enitoni/gears"
import { CommandGroup } from "@enitoni/gears-discordjs"

import { restartCommand } from "./restartCommand"
import { helpCommand } from "./helpCommand"
import { chanceGroup } from "./chance/chanceGroup"

export const prefixGroup = new CommandGroup({
	matcher: matchPrefixes("."),
	commands: [chanceGroup, restartCommand, helpCommand],
})
