import { CommandMatcher } from "@enitoni/gears"

const URL_REGEX = /[(http(s)?):\/\/(www\.)?a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/

export const matchUrl: () => CommandMatcher = () => async context => {
	const { content } = context
	const match = URL_REGEX.exec(content)

	context.content = context.content.replace(URL_REGEX, "").trim()

	if (match) return context
}
