import { Bot, matchAll } from "@enitoni/gears"
import { Adapter, CommandGroup } from "@enitoni/gears-discordjs"

import { ChannelLoggingService } from "./services/ChannelLoggingService"
import { ChanceService } from "./services/ChanceService"
import { prefixGroup } from "./commands/prefixGroup"
import { passiveCommand } from "./commands/passiveCommand"
import { handleErrors } from "./commands/middleware/handleErrors"

import "dotenv/config"

const adapter = new Adapter({ token: process.env.TOKEN! })

const bot = new Bot({
	adapter,
	services: [ChanceService, ChannelLoggingService],
	group: new CommandGroup({
		matcher: matchAll(),
		middleware: [handleErrors()],
		commands: [prefixGroup, passiveCommand],
	}),
})

bot.client.on("ready", () => bot.client.user?.setActivity("u"))

bot.start()
