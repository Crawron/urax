export function sanitize(str: string) {
	const spoiler = /\|\|([^|\n]+|\|)\|\|/g
	const newlineTab = /[\n\t]/g
	const repeatedSpace = /( ){2,}/g

	return removeMarkdown(str.replace(spoiler, ""))
		.replace(newlineTab, " ")
		.replace(repeatedSpace, " ")
		.trim()
}

function removeMarkdown(str: string) {
	const emojiRegexp = /:(\w|\-){2,}:/g
	const mdRegexp = /[*_>~`]/g

	return str
		.split(" ")
		.map(word => {
			if (emojiRegexp.test(word)) return word
			else return word.replace(mdRegexp, "")
		})
		.join(" ")
}
