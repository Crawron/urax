export function toPercentage(normal: number): string {
	return (normal * 100).toFixed(10).replace(/0*$/, "").replace(/\.$/, "") + "%"
}
