import { Permissions } from "discord.js"
import { humanizePermissions } from "./humanizePermissions"

test("humanize single perm", () => {
	const result = humanizePermissions(new Permissions("EMBED_LINKS"))
	expect(result).toBe("Embed Links")
})

test("humanize multiple perms", () => {
	const result = humanizePermissions(
		new Permissions(["KICK_MEMBERS", "BAN_MEMBERS"])
	)

	expect(result).toBe("Kick Members, Ban Members")
})
