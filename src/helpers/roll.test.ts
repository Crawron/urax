import { roll } from "./roll"

function testChance(chance: number, attempts: number) {
	let triggers = 0

	for (let i = 0; i < attempts; i++) {
		if (roll(chance)) triggers++
	}

	return triggers
}

describe("rolling", () => {
	test("no trigger on chance 0", () => {
		const attempts = 5000
		let triggers = testChance(0, attempts)
		expect(triggers / attempts).toBeCloseTo(0, 1)
	})

	for (let chance = 0.1; chance < 1; chance += 0.1) {
		test(`triggers when chance ${chance.toFixed(1)}`, () => {
			const attempts = 5000
			let triggers = testChance(chance, attempts)
			expect(triggers / attempts).toBeCloseTo(chance, 1)
		})
	}
})
