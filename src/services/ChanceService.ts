import { Service } from "@enitoni/gears-discordjs"
import { readFileSync, writeFile } from "fs"
import { promisify } from "util"

const writeFileAsync = promisify(writeFile)

export class ChanceService extends Service {
	private guilds!: ChanceStoredState
	private dataPath = "/data/guildConfig.json"

	async serviceDidInitialize() {
		try {
			this.guilds = await this.readStoredState()
		} catch (err) {
			this.guilds = {
				default: {
					chance: 0.01,
					rollingStyle: "classic",
				},
			}
			this.writeState()
		}
	}

	private async setGuildState(state: { [key: string]: GuildConfig }) {
		this.guilds = { ...this.guilds, ...state }
		this.writeState()
	}

	private async writeState() {
		writeFileAsync(this.dataPath, JSON.stringify(this.guilds, undefined, 2))
	}

	private async readStoredState() {
		const storedBuffer = readFileSync(this.dataPath, {
			encoding: "utf-8",
		})
		return JSON.parse(storedBuffer.toString())
	}

	get defaultConfig() {
		return this.guilds.default
	}

	getGuildConfig(guildId: string): GuildConfig {
		const guild = this.guilds[guildId]

		if (guild) return guild
		else return this.defaultConfig
	}

	setGuildConfig(guildId: string, config: Partial<GuildConfig>) {
		const guild: GuildConfig = this.getGuildConfig(guildId)
		const guildState = { [guildId]: { ...guild, ...config } }
		this.setGuildState(guildState)
	}
}

type ChanceStoredState = {
	default: GuildConfig
	[key: string]: GuildConfig | undefined
}

type GuildConfig = {
	chance: number
	rollingStyle: "classic" | "default"
}
